﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Net.Http;
using System.Net.Http.Headers;
using ECMToolbox.ADRefreshService.Classes;
using System.ComponentModel;
using System.Data;

namespace ECMToolbox.ADRefreshService
{
    class ADRefreshCycle
    {
        public static string ConnectionString { get; private set; }
        public static bool IsRunning { get; private set; }

        private static Thread tCycle;

        // Function to start the kickoff cycle
        internal void StartWorkflowKickoffCycle()
        {
            if (tCycle != null && tCycle.IsAlive) // If the thread is not empty and is alive
            {
                IsRunning = false;
                tCycle.Join(60000);
            }

            tCycle = new Thread(Cycle);
            IsRunning = true;
            tCycle.Start();
        }

        // Stop the cycle.
        internal void StopWorkflowKickoffCycle()
        {
            if (tCycle != null && tCycle.IsAlive)
            {
                IsRunning = false;
                tCycle.Join(60000);
            }
            tCycle = null;
        }
        // Cycle
        private void Cycle()
        {
            Log.Write("ECMToolbox AD Refresh Service v1.1.0 Service started");
            try
            {
                while (IsRunning) // While the application is running
                {
                    try
                    {
                        Log.Write("Starting Refresh thread...");
                        RefreshActiveDirectoryInfo(); // Perform function.
                        Log.Write("Refresh thread completed!");
                    }
                    catch (Exception ex)
                    {
                        Log.WriteError("Error while trying to refresh AD users: ", ex);
                    }

                    //Cycles Every 1 second to determine if it's time to sync again or if the service has been terminated
                    for (var i = 0; i < AppSettings.Settings.CycleSeconds && IsRunning; i = i + 1) Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                Log.WriteError("An Error has occured during the cycle process: ", ex);
            }
        }
        // Function to synchronize the Workflow users and groups.
        private static void RefreshActiveDirectoryInfo()
        {
            try
            {
                using (HttpClientHandler clientHandler = new HttpClientHandler() { UseProxy = false })
                {
                    clientHandler.UseDefaultCredentials = true;

                    using (HttpClient client = new HttpClient(clientHandler, false))
                    {
                        client.BaseAddress = new Uri(AppSettings.Settings.WorkflowWebServiceAddress);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        Log.Write("Logging into Workflow...");
                        WorkflowAPI.sessionID = WorkflowAPI.WorkflowLogin(client, AppSettings.Settings.WorkflowUser, Encryption.PasswordDecryptReversible(AppSettings.Settings.WorkflowPassword, "ssb1$32"));
                        Log.Write("Logged into Workflow. Session ID: " + WorkflowAPI.sessionID);
                        // Get Users and Groups From Active Directory
                        ActiveDirectory.ADInfo adInfo = ActiveDirectory.GetADInfo();
                        if (AppSettings.Settings.Debug_Dryrun)
                        {
                            // Let's kill the thread if dry run is enabled.
                            Log.Write("Thread Terminated due to dry run setting being enabled.");
                            tCycle.Suspend();
                        }

                        // Loop Keep alive Session
                        var timer = new System.Threading.Timer((e) =>
                        {
                            if (WorkflowAPI.sessionID != null)
                            {
                                WorkflowAPI.WorkflowKeepAlive(AppSettings.Settings.WorkflowWebServiceAddress, WorkflowAPI.sessionID);
                                Log.Write("Sent Workflow Keepalive Request.");
                            }
                        }, null, TimeSpan.Zero, TimeSpan.FromMinutes(1)
                        );

                        // Get Full User List from WF WebServices
                        IEnumerable<ECMTUser> wfUsers = WorkflowAPI.GetWFUsers(client, WorkflowAPI.sessionID);

                        // Filter list for AD (NT) users
                        wfUsers = wfUsers.Where(x => x.UserType == "NT").ToList().AsEnumerable();

                        // Create new users
                        // for each user in active directory where wf username uppercased equals ad username uppercased.
                        foreach (UserPrincipal newUser in adInfo.Users.Where(x => wfUsers.Where(y => y.Username.ToUpper() == x.SamAccountName.ToUpper()).Count() == 0))
                        {
                            try
                            {
                                ECMTUser newECMTUser = new ECMTUser();
                                Log.Write(newECMTUser.Username);
                                newECMTUser.Username = newUser.SamAccountName;
                                newECMTUser.FullName = newUser.DisplayName;
                                newECMTUser.UserType = "NT";
                                newECMTUser.Email = newUser.EmailAddress;
                                newECMTUser.IsEnabled = true;
                                Log.Write("Setting User Manager rights for user: " + newECMTUser.Username);
                                newECMTUser.UserManager = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWFUserManager).Users.Where(x => x.SamAccountName.ToUpper() == newUser.SamAccountName.ToUpper()).Count() > 0);
                                Log.Write("Setting Workflow Manager rights for user: " + newECMTUser.Username);
                                newECMTUser.WorkflowManager = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWDDesignerGroup).Users.Where(x => x.SamAccountName.ToUpper() == newUser.SamAccountName.ToUpper()).Count() > 0);
                                Log.Write("Setting Administration rights for user: " + newECMTUser.Username);
                                newECMTUser.SystemAdministrator = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWFAdminGroup).Users.Where(x => x.SamAccountName.ToUpper() == newUser.SamAccountName.ToUpper()).Count() > 0);
                                Log.Write("Attempting to Add WF User: " + newECMTUser.Username);
                            
                                WorkflowAPI.AddWFUser(client, WorkflowAPI.sessionID, newECMTUser);
                            }
                            catch (Exception ex)
                            {
                                Log.WriteError("An Error has occured while adding a new user: ", ex);
                            }
                        }

                        // Set WF Roles Based on AD Group Membership (Disable: user is not a member of any WF groups)
                        foreach (ECMTUser wfUser in wfUsers)
                        {
                            try
                            {
                                wfUser.IsDirty = false;
                                wfUser.IsEnabled = (adInfo.Users.Where(x => x.SamAccountName.ToUpper() == wfUser.Username.ToUpper()).Count() > 0)
                                        && (adInfo.DisabledUsers.Where(x => x.SamAccountName.ToUpper() == wfUser.Username.ToUpper()).Count() == 0);

                                if (wfUser.IsEnabled) // Don't bother updating anything else if user is disabled.
                                {
                                    wfUser.UserManager = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWFUserManager).Users.Where(x => x.SamAccountName.ToUpper() == wfUser.Username.ToUpper()).Count() > 0);
                                    wfUser.WorkflowManager = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWDDesignerGroup).Users.Where(x => x.SamAccountName.ToUpper() == wfUser.Username.ToUpper()).Count() > 0);
                                    wfUser.SystemAdministrator = (adInfo.ADGroups.Single(x => x.GroupName == AppSettings.Groups.ADWFAdminGroup).Users.Where(x => x.SamAccountName.ToUpper() == wfUser.Username.ToUpper()).Count() > 0);
                                }

                                if (wfUser.IsDirty)
                                {
                                    Log.Write("Checking for user: " + wfUser.Username);
                                    WorkflowAPI.UpdateWFUser(client, WorkflowAPI.sessionID, wfUser);
                                    Log.Write("Checked/Modified: " + wfUser.Username);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.WriteError("An Error has occured while updating a user: ", ex);
                            }
                        }

                        // Refresh User List (need to get user ids for new users). Refreshing list after user updates to minimize AD calls.
                        wfUsers = WorkflowAPI.GetWFUsers(client, WorkflowAPI.sessionID);
                        wfUsers = wfUsers.Where(x => x.UserType == "NT").ToList().AsEnumerable();

                        // Get Full Group List from WF WebServices
                        IEnumerable<ECMTGroup> wfGroups = WorkflowAPI.GetWFGroups(client, WorkflowAPI.sessionID);
                        // Filter group list to contain AD groups only
                        wfGroups = wfGroups.Where(x => adInfo.ADGroups.Where(y => y.GroupName.ToUpper() == x.GroupName.ToUpper()).Count() >= 1).ToList().AsEnumerable();
                        // Set WF Group Membership Based on AD Group Membership
                        foreach (ECMTGroup wfGroup in wfGroups)
                        {
                            wfGroup.IsDirty = false;
                            try
                            {
                                // Each WF Group, get the members that are in the AD Groups and acknowledge if the user is found in that AD Group.
                                wfGroup.Users = wfGroup.Users.ToList().Union
                                    (wfUsers.Where(x => adInfo.ADGroups.SingleOrDefault(z => z.GroupName.ToUpper() == wfGroup.GroupName.ToUpper()).Users.Where
                                    (y => y.SamAccountName.ToUpper() == x.Username.ToUpper()).Count() > 0).Select(x => x.ID).ToArray()).ToArray();

                                if (wfGroup.IsDirty)
                                {
                                    Log.Write("Checking for Group: " + wfGroup.GroupName);
                                    WorkflowAPI.UpdateWFGroupMembers(client, WorkflowAPI.sessionID, wfGroup);
                                    Log.Write("Checked/Modified Group: " + wfGroup.GroupName);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.WriteError("Error Occured while trying to update group changes.", ex);
                            }
                        }
                        Log.Write("Logging out of Workflow...");
                        WorkflowAPI.WorkflowLogout(client, WorkflowAPI.sessionID);
                        Log.Write("Logged out of Workflow.");
                        WorkflowAPI.sessionID = null;
                    }
                }
            }
            catch(Exception ex)
            {
                Log.WriteError("An Error has occured while running the Workflow 4 CRUD transaction: ", ex);
            }
        }
    }
}
