﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ECMToolbox.ADRefreshService.Framework;
using ECMToolbox.ADRefreshService.Classes;

namespace ECMToolbox.ADRefreshService
{
    public static class Program
    {
        // The main entry point for the windows service application.
        static void Main(string[] args)
        {
            try
            {
                // If install was a command line flag, then run the installer at runtime.
                if (args.Contains("-install", StringComparer.InvariantCultureIgnoreCase))
                {
                    WindowsServiceInstaller.RuntimeInstall<ServiceImplementation>();
                }

                // If uninstall was a command line flag, run uninstaller at runtime.
                else if (args.Contains("-uninstall", StringComparer.InvariantCultureIgnoreCase))
                {
                    WindowsServiceInstaller.RuntimeUnInstall<ServiceImplementation>();
                }

                // Otherwise, fire up the service as either console or windows service based on UserInteractive property.
                else
                {
                    var implementation = new ServiceImplementation();

                    // If started from console, file explorer, etc, run as console app.
                    if (Environment.UserInteractive)
                    {
                        RunDebugMode();
                        ConsoleHarness.Run(args, implementation);
                    }
                    // Otherwise run as a windows service
                    else
                    {
                        ServiceBase.Run(new WindowsServiceHarness(implementation));
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleHarness.WriteToConsole(ConsoleColor.Red, "An exception occurred in Main(): {0}", ex);
            }

        }
        public static void RunDebugMode()
        {
            Console.Clear();
            Console.Title = "ECMT AD Sync Tool v1.1.0 | Debug Mode";
            Console.Write("This service should only be ran as a console for debugging purposes.\nPress a key corresponding to the action.\n[D]ebug mode\n[S]et Password for Workflow account.\n");
            string result = Console.ReadLine().ToUpper();

            if (result == "D")
            {
                Console.Clear();
                Console.WriteLine("Starting...");
            }
            else if (result == "S")
            {
                Console.Clear();
                Console.WriteLine("Please type in the new password to set for the Workflow account in the config file.");
                Console.Write("New Password: ");
                // Get the password from the user.
                string password = Console.ReadLine();
                AppSettings.UpdateSetting("Settings", "WorkflowAPI_Password", Encryption.PasswordEcryptReversible(password, "ssb1$32"));
                Console.Beep();
                Console.WriteLine("Password encrypted and saved successfully!\n You must restart the application for the changes to apply.");
                Environment.Exit(-1);
                RunDebugMode();

            }
            else if (result == "T")
            {
                Console.WriteLine("Config setting for Cycle seconds: " + (int)AppSettings.GetSetting()["Settings"]["CycleSeconds"] + "\n Press any key to continue...");
                Console.ReadLine();
                RunDebugMode();
            }
            else
            {
                Console.WriteLine("Invalid command, please try again,");
                RunDebugMode();
            }
        }
    }
}
