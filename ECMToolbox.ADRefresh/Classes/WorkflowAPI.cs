﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ECMToolbox.ADRefreshService.Classes
{
    public class WorkflowAPI
    {
        public static string sessionID { get; set; }
        #region WORKFLOW API

        internal static string WorkflowLogin(HttpClient client, string WorkflowUser, string WorkflowPassword)
        {
            Result result = null;
            HttpResponseMessage response = client.PostAsync("Login/" + WorkflowUser + "/" + WorkflowPassword, null).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
            return result.Session.SessionID;
        }

        internal static bool WorkflowLogout(HttpClient client, string sessionID)
        {
            Result result = null;
            HttpResponseMessage response = client.PostAsync("Logout/" + sessionID, null).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
            return true;
        }

        internal static bool WorkflowKeepAlive(string WorkflowWebServiceAddress, string sessionID)
        {
            using (HttpClientHandler clientHandler = new HttpClientHandler() { UseProxy = false })
            {
                clientHandler.UseDefaultCredentials = true;

                using (HttpClient client = new HttpClient(clientHandler, false))
                {
                    client.BaseAddress = new Uri(WorkflowWebServiceAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Result result = null;
                    HttpResponseMessage response = client.PostAsync("SessionKeepAlive/" + sessionID, null).GetAwaiter().GetResult();
                    response.EnsureSuccessStatusCode();
                    result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
                    if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
                    return true;
                }
            }
        }

        internal static IEnumerable<ECMTUser> GetWFUsers(HttpClient client, string sessionID)
        {
            Result result = null;
            string method = "GetUsers/" + sessionID;
            HttpResponseMessage response = client.GetAsync(method).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();

            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);

            return (IEnumerable<ECMTUser>)Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ECMTUser>>(result.Data.ToString());
        }

        internal static IEnumerable<ECMTGroup> GetWFGroups(HttpClient client, string sessionID)
        {
            Result result = null;
            string method = "GroupsGet/" + sessionID;
            HttpResponseMessage response = client.GetAsync(method).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);

            return (IEnumerable<ECMTGroup>)Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ECMTGroup>>(result.Data.ToString());
        }

        internal static void AddWFUser(HttpClient client, string sessionID, ECMTUser newECMTUser)
        {
            Result result = null;
            string method = "CreateUser/" + sessionID;
            HttpResponseMessage response = client.PostAsJsonAsync(method, newECMTUser).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
        }

        internal static void UpdateWFUser(HttpClient client, string sessionID, ECMTUser ecmtUser)
        {
            Result result = null;
            string method = "UpdateUser/" + sessionID;
            HttpResponseMessage response = client.PostAsJsonAsync(method, ecmtUser).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
        }

        internal static void UpdateWFGroupMembers(HttpClient client, string sessionID, ECMTGroup ecmtGroup)
        {
            Result result = null;
            string method = "GroupInfoUpdateUsers/" + sessionID;
            HttpResponseMessage response = client.PutAsJsonAsync(method, ecmtGroup).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            result = response.Content.ReadAsAsync<Result>().GetAwaiter().GetResult();
            if (result.Messages.ErrorMessages.Count > 0) throw new Exception(result.Messages.ErrorMessages[0]);
        }
        #endregion
    }
    public class Result
    {
        private int total = 0;
        private object data;

        public object Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }
        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }
        public Message Messages { get; set; } = new Message();

        public string Stack { get; set; }

        public SessionView Session { get; set; } = new SessionView();
    }

    public class SessionView
    {
        public string SessionID { get; set; }
        public int UserID { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string UserType { get; set; }
        public bool IsSystemAdmin { get; set; }
        public bool IsUserManager { get; set; }
        public bool IsWorkflowManager { get; set; }
        public bool ImpersonationEnabled { get; set; }
    }

    public class Message
    {
        public List<string> SuccessMessages { get; set; } = new List<string>();
        public List<string> InfoMessages { get; set; } = new List<string>();
        public List<string> FailureMessages { get; set; } = new List<string>();
        public List<string> ErrorMessages { get; set; } = new List<string>();
    }
       public class ECMTUser : INotifyPropertyChanged
    {
        private bool isenabled;
        private bool usermanager;
        private bool workflowmanager;
        private bool systemadministrator;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            this.IsDirty = true;
        }

        public int ID { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        #region sysadmin declaration
        public bool SystemAdministrator
        {
            get
            {
                return this.systemadministrator;
            }
            set
            {
                if (this.systemadministrator != value)
                {
                    systemadministrator = value;
                    OnPropertyChanged("SystemAdministrator");
                }
            }
        }
        #endregion

        public bool WorkflowManager
        {
            get
            {
                return this.workflowmanager;
            }
            set
            {
                if (this.workflowmanager != value)
                {
                    workflowmanager = value;
                    OnPropertyChanged("WorkflowManager");
                }
            }
        }
        public bool UserManager
        {
            get
            {
                return this.usermanager;
            }
            set
            {
                if (this.usermanager != value)
                {
                    usermanager = value;
                    OnPropertyChanged("UserManager");
                }
            }
        }
        public bool IsEnabled
        {
            get
            {
                return this.isenabled;
            }
            set
            {
                if (this.isenabled != value)
                {
                    isenabled = value;
                    OnPropertyChanged("IsEnabled");
                }
            }
        }
        public bool IsDirty { get; set; }
    }

    public class ECMTGroup : INotifyPropertyChanged
    {
        private int[] users;

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            if (propertyName != "IsDirty")
            {
                this.IsDirty = true;
            }
        }

        public short GroupID { get; set; }
        public string GroupName { get; set; }
        public int[] Users
        {
            get
            {
                return this.users;
            }
            set
            {
                if (this.users != value)
                {
                    users = value;
                    OnPropertyChanged("Users");
                }
            }
        }
        public bool IsDirty { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
