﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ECMToolbox.ADRefreshService.Classes
{
    // This class is responsible for handling all application settings.
    // Author: Daniel Shor, Aric Malsbury
    public class AppSettings
    { 
        // TODO: Connect Json file parsing and get data from Config and load into application.
        private static string ConfigFile = AppDomain.CurrentDomain.BaseDirectory + "config.json";
        private static JObject jsondata;
        private static bool jsonloaded = false;

        public class Settings
        {
            public static int CycleSeconds = (int)GetSetting()["Settings"]["CycleSeconds"];
            public static string WorkflowWebServiceAddress = (string)GetSetting()["Settings"]["WorkflowAPI_URL"];
            public static string WorkflowUser = (string)GetSetting()["Settings"]["WorkflowAPI_User"];
            //= (string)GetSetting()["Settings"]["WorkflowAPI_Password"];
            public static string WorkflowPassword = (string)GetSetting()["Settings"]["WorkflowAPI_Password"];
            public static string ADDomain = (string)GetSetting()["Settings"]["ADDomainName"];
            public static string ADWFOrgUnit = (string)GetSetting()["Settings"]["ADWorkflowOrgUnits"];
            public static string ADWFDisabledOrgUnit = (string)GetSetting()["Settings"]["ADWorkflowDisabledOrgUnit"];
            public static bool Debug_Enable = (bool)GetSetting()["Settings"]["Debug_Mode"];
            public static bool Debug_Dryrun = (bool)GetSetting()["Settings"]["Debug_Dryrun"];
        }
        public class Groups
        {
            public static string ADWFAdminGroup = (string)GetSetting()["Groups"]["Admin_GroupName"];
            public static string ADWDDesignerGroup = (string)GetSetting()["Groups"]["Designer_GroupName"];
            public static string ADWFUserManager = (string)GetSetting()["Groups"]["UserManager_GroupName"];
            public static string ADWFAllUsers = (string)GetSetting()["Groups"]["WorkflowUsers_GroupName"];
            public static string ADWFDisabledUsers = (string)GetSetting()["Groups"]["WorkflowUsersDisabled_GroupName"];
        }
        // Function to expose Json Objects to bind to the class objects.
        // NOTE: This is a workaround due to time constraints, wish list is to get a proper framework written to handle Json configuration professionally.
        public static JObject GetSetting()
        {
            // Check if the data is already loaded, so we only read from the file once.
            if (jsonloaded)
            {
                return jsondata;
            }
            else
            {
                using (StreamReader rdr = new StreamReader(ConfigFile))
                {
                    // Instatiate the config file
                    jsonloaded = true;
                    return jsondata = JObject.Parse(File.ReadAllText(ConfigFile));
                }
            }
        }
        // Function to reload the settings into the JObject.
        public static JObject ReloadSettings()
        {
            Log.Write("Reloading settings...");
            using (StreamReader rdr = new StreamReader(ConfigFile))
            {
                // Instatiate the config file
                return jsondata = JObject.Parse(File.ReadAllText(ConfigFile));
            }
        }

        // Setting to update a value in Json Config.
        // For now the configuration hiearchy has groups, then the children objects are the actual setting values. Good to categorize settings.
        public static void UpdateSetting(string SettingGroup, string settingname, string settingvalue)
        {
            try
            {
                string json = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Config.json");
                JObject jsonobject = JObject.Parse(json);
                // TODO: Validation with data types.
                Log.Write("Attempting to update setting name: " + settingname + " Value: " + settingvalue);
                jsonobject[SettingGroup][settingname] = settingvalue;
                Log.Write("Setting: " + settingname + " Has been updated!");
                // Let's save the value back into the JSON file.
                using (StreamWriter sw = File.CreateText(AppDomain.CurrentDomain.BaseDirectory + "Config.json"))
                {
                    using (JsonTextWriter writer = new JsonTextWriter(sw))
                    {
                        writer.Formatting = Formatting.Indented;
                        jsonobject.WriteTo(writer);
                    }
                }
                // Reload the config file.
            }
            catch (Exception e) {
                Log.WriteError("There was an error updating setting: " + settingname + "\n Error: ", e);
            }


        }
    }
}
