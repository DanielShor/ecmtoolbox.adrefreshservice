﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECMToolbox.ADRefreshService.Classes
{
    // This class is responsible for handling all AD Inqueiries.
    // Author: Daniel Shor, Aric Malsbury
    public class ActiveDirectory
    {
        // Function to get Active Directory information.
        public static ADInfo GetADInfo()
        {
            ADInfo adInfo = new ADInfo();
            List<ADGroup> adGroups = new List<ADGroup>();
            List<ADGroup> adGroupsDisabled = new List<ADGroup>();

            PrincipalContext pContext = new PrincipalContext(ContextType.Domain, AppSettings.Settings.ADDomain, AppSettings.Settings.ADWFOrgUnit);
            IEnumerable<GroupPrincipal> gPrincipalsDisabled;
            IEnumerable<UserPrincipal> uPrincipalsDisabled;

            if (AppSettings.Settings.ADWFDisabledOrgUnit != "")
            {
                // Disabled context for disabled users.
                PrincipalContext pContextDisabled = new PrincipalContext(ContextType.Domain, AppSettings.Settings.ADDomain, AppSettings.Settings.ADWFDisabledOrgUnit);
                GroupPrincipal gPrincipalDisabled = new GroupPrincipal(pContextDisabled, "*");
                UserPrincipal uPrincipalDisabled = new UserPrincipal(pContextDisabled);
                PrincipalSearcher psDisabled = new PrincipalSearcher(gPrincipalDisabled);
                PrincipalSearcher psUDisabled = new PrincipalSearcher(uPrincipalDisabled);
                gPrincipalsDisabled = psDisabled.FindAll().Cast<GroupPrincipal>();
                uPrincipalsDisabled = psUDisabled.FindAll().Cast<UserPrincipal>();
            }
            else
            {
                gPrincipalsDisabled = null;
                uPrincipalsDisabled = null;
            }


            // Here get every group in the org unit
            GroupPrincipal gPrincipal = new GroupPrincipal(pContext, "*");


            // The searcher object is created
            PrincipalSearcher ps = new PrincipalSearcher(gPrincipal);

            IEnumerable<UserPrincipal> uPrincipals = null;
            IEnumerable<GroupPrincipal> gPrincipals;


            // Putting group results into an enumerable object.
            gPrincipals = ps.FindAll().Cast<GroupPrincipal>();

            adInfo.DisabledUsers = Enumerable.Empty<UserPrincipal>();

            // For each group in enumerable object, 
            foreach (GroupPrincipal myGroup in gPrincipals)
            {
                Console.WriteLine("Searching for: " + myGroup.Name);
                // Construct an enumerable collection of group members found in the group.
                IEnumerable<UserPrincipal> groupMembers = GetAllGroupMembers(myGroup);

                // If group is WF-AllWfUsers then add members to WF user list
                //if (myGroup.Name == AppSettings.Groups.ADWFAllUsers) {
                //    uPrincipals =  groupMembers;
                //}
                //else if (myGroup.Name == AppSettings.Groups.ADWFDisabledUsers) {
                //    adInfo.DisabledUsers = groupMembers;
                //}

                if (uPrincipals == null)
                {
                    uPrincipals = groupMembers;
                }
                else
                {
                    uPrincipals = uPrincipals.Union(groupMembers, new UserPrincipalComparer());
                }

                ADGroup adGroup = new ADGroup();
                // Map groupname and users to appropriate objects.
                adGroup.GroupName = myGroup.Name;
                adGroup.Users = groupMembers;
                adGroups.Add(adGroup);
            }
            if (AppSettings.Settings.ADWFDisabledOrgUnit != "")
            {
                // Add disabled users from groups within disabled org unit
                foreach (GroupPrincipal igroup in gPrincipalsDisabled)
                {
                    IEnumerable<UserPrincipal> disabledGroupMembers = GetAllGroupMembers(igroup);
                    adInfo.DisabledUsers = adInfo.DisabledUsers.Union(disabledGroupMembers, new UserPrincipalComparer());
                }

                // Add disabled users from disabled org unit
                adInfo.DisabledUsers = adInfo.DisabledUsers.Union(uPrincipalsDisabled, new UserPrincipalComparer());
            }


            adInfo.Users = uPrincipals;
            adInfo.ADGroups = adGroups;

            return adInfo;
        }
        // Function to get all group members.
        public static IEnumerable<UserPrincipal> GetAllGroupMembers(GroupPrincipal adGroup)
        {
            List<GroupPrincipal> adGroups = new List<GroupPrincipal>();
            adGroups.Add(adGroup);

            return GetAllGroupMembers(adGroups.AsEnumerable());
        }
        // Function to get all group members with IEnumerable.
        public static IEnumerable<UserPrincipal> GetAllGroupMembers(IEnumerable<GroupPrincipal> adGroups)
        {
            IEnumerable<UserPrincipal> userMembers = Enumerable.Empty<UserPrincipal>();
            Log.Write("Total sub groups: " + adGroups.Count());

            foreach (GroupPrincipal adGroup in adGroups)
            {
                Log.Write("Getting group members: " + adGroup.Name);

                userMembers = userMembers.Union(adGroup.Members.OfType<UserPrincipal>());
                Log.Write("Count for group: " + userMembers.Count());

                IEnumerable<GroupPrincipal> subGroups = adGroup.Members.OfType<GroupPrincipal>();
                if (subGroups.Count() > 0)
                {
                    userMembers = userMembers.Union(GetAllGroupMembers(subGroups));
                }
            }
            Log.Write("Total Count for group: " + userMembers.Count());
            return userMembers;
        }

        public class ADInfo
        {
            public IEnumerable<UserPrincipal> Users { get; set; }
            public IEnumerable<UserPrincipal> DisabledUsers { get; set; }
            public IEnumerable<ADGroup> ADGroups { get; set; }
        }

        public class ADGroup
        {
            public string GroupName { get; set; }
            public IEnumerable<UserPrincipal> Users { get; set; }
        }

        public class UserPrincipalComparer : IEqualityComparer<UserPrincipal>
        {
            public bool Equals(UserPrincipal u1, UserPrincipal u2)
            {
                if (u1 == null && u2 == null)
                    return true;
                else if (u1 == null || u2 == null)
                    return false;
                else if (u1.SamAccountName == u2.SamAccountName)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(UserPrincipal uP)
            {
                if (uP == null)
                    return 0;
                else
                {
                    return uP.SamAccountName.GetHashCode();
                    //byte[] hSamName = Encoding.ASCII.GetBytes(uP.SamAccountName);
                    //return BitConverter.ToInt32(hSamName, 0);
                }
            }
        }
    }
}
