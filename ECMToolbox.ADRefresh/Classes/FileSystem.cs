﻿using System;
using System.IO;

namespace ECMToolbox.ADRefreshService.Classes
{
    public class FileSystem
        // Handle filesystem functions such as creating, modifying, and reading files.
    {
        public static string rootpath = AppDomain.CurrentDomain.BaseDirectory;

        // Function to create Log path if not exist.
        public static void CreateLogPath()
        {
            bool exists = Directory.Exists(rootpath + "/Logs");
            if (!exists)
            {
                Directory.CreateDirectory(rootpath + "/Logs");
            }
        }

    }
}
