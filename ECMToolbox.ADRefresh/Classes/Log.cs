﻿using System;
using System.IO;

namespace ECMToolbox.ADRefreshService.Classes
{
    // Function to log out to console or database table.
    public static class Log
    {
        private static string textfile = "Debug_" + DateTime.Now.ToString("MM-dd-yy") + ".txt";
       
        // Internal function to write to text file.
        private static void WriteDebugLog(string messageline)
        {
            FileSystem.CreateLogPath();
            File.AppendAllText(FileSystem.rootpath + "/Logs/" + textfile, messageline + "\n");
        }
        // Function to write general information.
        public static void Write(string msg)
        {
            string formattext = DateTime.Now + " [INFO]: " + msg;
            if (AppSettings.Settings.Debug_Enable == true) { WriteDebugLog(formattext); }
            Console.WriteLine(formattext);
        }
        // Function to write error and include stack if provided.
        public static void WriteError(string msg)
        {
                string formattext = DateTime.Now + " [ERROR]: " + msg;
                Console.WriteLine(formattext);
                if (AppSettings.Settings.Debug_Enable == true) { WriteDebugLog(formattext); }

        }
        public static void WriteError(string msg, object stack)
        {
                string formattext = DateTime.Now + " [ERROR]: " + msg + "\nStack: " + stack.ToString();
                Console.WriteLine(formattext);
                if (AppSettings.Settings.Debug_Enable == true) { WriteDebugLog(formattext); }
        }
    }
}
